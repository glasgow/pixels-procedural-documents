# Pixels Procedural Documents 

Repository to make SOP documentation

## Adding content

Documents are written using the _markdown_ format ([cheatsheet](https://www.markdownguide.org/cheat-sheet/)). To add content you do *not* have to compile or run anything. Just upload or edit _markdown_ files.

To add information to the documents:

1. Clone the repository:
> git clone https://gitlab.cern.ch/glasgow/pixels-procedural-documents.git

2. Navigate to _docs_ directory
> cd pixels-procedural-documents/docs

3. Add content (instructions for commandline, but alternatives should work):

    a. Edit an existing file
    - select existing directory

        - check existing 
        > ls 
        - change directory
        > cd EXISTING_DIRECTORY

    - select and edit existing file using any text editor (e.g. nano)

        - check existing 
        > ls 
        - open file using any text editor (e.g. nano)
        > nano XXX_FILE.md

    b. Add a new page to existing theme (directory)
    - select existing directory

    > ls 
    > cd EXISTING_DIRECTORY

    - add a new _markdown_ file (with ".md" extension) using any text editor (e.g. nano)
    > nano XXX_NEW_FILE.md

    c. Add a file in a new theme (directory)

    - add a directory to _docs_
    > mkdir NEW_DIRECTORY

    - add a new _markdown_ file (with ".md" extension) using any text editor (e.g. nano)        
    > nano XXX_NEW_FILE.md

4. Update repository
    - add (all) changes
    > git add --all
    - commit with message
    > git commit -m "A_USEFUL_MESSAGE"
    - push (to origin master)
    > git push

Updates to the remote master branch will automatically prompt deployment scripts. The documentation should update within minutes.
