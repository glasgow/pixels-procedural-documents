# Power Supply Settings

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2023.12.06 | Kenneth Wraight | Created document |

Describes the procedures to set-up Power Supply on Linux PC.

## ITSDAQ Automation Files

ITSDAQ documentation: [here](https://atlas-strips-itsdaq.web.cern.ch/power_supplies.html)

At Glasgow, ITSDAQ power supplies files can be found:

- /nfs/atlas/itk-atlas/strips/data/Electrical/
    - Single module tests : SModule/SCTVAR/config/power_supllies.json
    - Thermal Cycle tests: TCModule/SCTVAR/config/power_supllies.json

### setting USB connections

ITSDAQ documentation: [here](https://atlas-strips-itsdaq.web.cern.ch/power_supplies.html#identifying-the-device-path)

Check list of rules:

> less /etc/udev/rules.d


## HV interlock

The Cambridge interlock box contains a programmable arduino.

Can test/set communication via arduino application on PPEPC65:
- open application
- plug in USB from interlock

Instructions on communication with the arduino [here](https://www.hep.phy.cam.ac.uk/~hommels/interlock/InterlockCommands.htm)

### Running with ITSDAQ

