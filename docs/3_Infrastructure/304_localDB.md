# Flex Visual Inspection

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2024.02.28 | Kenneth Wraight | Created document |

## Scope

This is a regular part of testing pixels front end chips at Glasgow.


## Purpose

ITk Pixels preferred tool for electrical testing


## Responsibilities

People: Dima


## Equipment

In lab:

- YARR set-up:

    - LV PS
    - HV PS

- PC: can be connected outside of lab
    - localDB

## Set-up

LocalDB runs on lab server [here](http://194.36.1.20:5000/localdb/)

- IP: 194.13.1.20
- port: 5000

Using mongo database [here](http://194.36.1.20:27017))

- IP: 194.13.1.20
- port: 27017

__documentation__

[LocalDB documentation](https://atlas-itk-pixel-localdb.web.cern.ch): Pixels electrical test uploads

## Procedure

1. Connect to localDB on server: [localDB]((http://194.36.1.20:5000/localdb/)) 

<figure markdown>
   ![image info](/img/304_localDB/localdb-top.png){ width="400" }
  <figcaption>Top screen - login via _Sign in_ dropdown from top righthand side</figcaption>
</figure>

2. Login

Enter localDB login details:

<figure markdown>
   ![image info](/img/304_localDB/localDB-login.png){ width="400" }
  <figcaption>Login dropdown from top righthand side</figcaption>
</figure>

3. Pull components from Production Database

Enter (partial) serialNumber to match components and download information.

<figure markdown>
   ![image info](/img/304_localDB/localDB-login.png){ width="400" }
  <figcaption>Login dropdown from top righthand side</figcaption>
</figure>




