# Site Qualification

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2024.02.28 | Kenneth Wraight | Created document |

## Scope

Qualification of all sites building pixel modules 

## Purpose

Where to find Site Qualificaiton information


## Responsibilities

People: Richard, Kenny


## Links & Documentation

Site Qualification document: [googleDoc](https://docs.google.com/spreadsheets/d/1mCRBw6FqDFyPDkdJYjiWVeOllYERfKDbsypDy-LK7Uk/edit#gid=0)

- includes definitions of blocks (PDB stages) and sub-blocks (PDB tests) with contact details and responsible people per institution 

Site Qualification [dashboard](https://wraight.web.cern.ch/pixels-reports/dashboard_P-MODULESQ.html)

- contains site specific and aggregate project information

Site Qualification [webApp](https://wraight.web.cern.ch/pixels-reports/dashboard_P-MODULESQ.html)

- this is part of general pixels webApp under the _SQApp_ theme


## Procedure

Upload test with relevant information (link to documentation and compoenent serialNumber (if required))
