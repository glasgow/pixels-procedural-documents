# Nikon Camera

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2024.02.29 | Kenneth Wraight | Created document |

## Scope

Used by several components for Visual Inspection tests

## Purpose

How to take, manipulate and store images

## Responsibilities

People: Richard

## Equipment

Nikon D850 Camera in GLADD1

## Procedure


### Taking Images

- Ensure the stage of the camera stand is level before imaging
    - The spirit level sitting on the stage will indicate when it’s level. 

- Twist the dial (connected to the silver button) on the top right of the camera to the “On” position. 
    - If the green LED on the back of the camera doesn’t blink on when the camera is switched on, then it might be out of charge. The camera should be powered via the mains adapter.  

- Press the “Lv” button on the back of the camera (bottom right) to put the camera into Live view. 

- Place your sample on the stage and rotate the barrel of the lens to gauge whether it can focus on your sample. 
    - If the lens can’t focus on your sample or get in close enough to fill a sizeable amount of the LCD display, use the Z-stage to move the part closer. 

- Turn the lens anti-clockwise (as viewed from the back of the camera) to focus on items closer to the camera. 

- Use the “+ magnifying glass” button on the left-hand side of the back of the camera to zoom into the point of your sample that is closest to the lens. 
    - Fine focus on this by rotating the barrel of the lens and zoom back out using the “– magnifying glass” button. 

- Press “Menu” button on top left-hand corner of the back of the camera and select the “Focus Shift Shooting” option. 
    - If this option is greyed out, then it’s probably because the camera is in a state where focus shift shooting is unavailable. 
    - If so, check the scroll wheel on the top left of the camera and ensure it is set to S (single shot). 
    - If it is not set to S, press and hold down the black button next to the wheel and rotate until S is selected. 
    - For more details about the settings available in the focus shift shooting menu, visit the [website](https://nps.nikonimaging.com/technical_info/technical_solutions/d850_tips/useful/focus_stacking/)


- Once you’ve chosen your settings for focus shift shooting and are happy with the lighting conditions for the photography
    - turn out lights in the room and use lights on camera stand with white controller to test different lighting conditions

- press the “Start” button using the black button in the center right of the back of the camera. 
    - Let the camera take the number of shots that you have defined in your settings. 

### Saving and Stacking Images

- Once the photos have been taken, plug the labelled USB cable from the PC next to the camera stand into the 2nd flap on the left side of the camera. 
    - If the rubber flap is closed, flip it open. Switch the camera on like before. 

- The camera should now be connected to the PC as an external device. You can now access the images in the folders on the camera, cut them from the folder on the camera and paste them into your chosen storage folder on the PC. 
    - Cutting and pasting the images from the camera ensure the memory card doesn’t get clogged up with unwanted photos further down the line. 
 
- Open Photoshop, account details if asked are  
    - User: calum.gray@glasgow.ac.uk 
    - Password: ParticlePhysics@123 
  

- Go to __File> Scripts> Load Files into Stack__. 

- Browse and select all the photos that you want to stack and click OK.
     - Let Photoshop open all the files. This takes more time depending on how big the files are and how many photos you want to stack. 
 
- Once all your photos have loaded in Photoshop, _Shift-click_ from the top of the list to the bottom in the bottom right-hand pane and select __Edit > Auto-Blend Layers__. 
    - Let Photoshop perform the image stacking. Again, the time it takes to do this will vary. 
 
- Use the Zoom (magnifying glass) and Grabber/Pan (hand) tool on the left-hand tool bar in Photoshop to inspect the image stack post-processing. You should see the whole image in focus. 
 
- Once happy after inspecting, click __File > Save as__ and name the stack, select the file type you want and save to the destination of your choice. 
    - Recommended apps for viewing the stacked image after saving are _Irfanview_ and _Windows Photo Viewer_ (pre-windows 10 computers), however this is all down to personal preference. 
