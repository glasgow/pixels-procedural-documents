# IV Procedure 


## Hardware set-up 

Keithley 237 wiring:

- Chuck - RHS to GND 
- Probe 2 - RHS to GND 
- Probe 4 - LHS to HV 

## Proceedure

### Pre-setup

- Turn on all instruments under the Wentworth and K237 in the rack 

    - Light needs to be on to use video card 

- Use Joystick controller to set system into remote 

- Start LabMaster FA on PC to RHS of Wentworth 


__Items to be done__

- Use a custom chuck 

- Make a new PT100 temperature sensor and secure to chuck 

 
### Labview Programs to run 
 
> D:\gladd-labview\keithley-2700\K2700_T_RH_Monitor_v1.0.1.vi 

> D:\gladd-labview\keithley-237\CurrentVoltageMeasurer_K237_v1.3.3.vi 

> D:\gladd-labview\SensorMarketSurvey_GUI_Assist_v1.3.1.vi 


1. set-up K2700_T_RH_Monitor_v1.0.1.vi  

- Don't do anything 

2. set-up CurrentVoltageMeasurer_K237_v1.3.3.vi 

- Compliance: 100uA 
- NMeasurements: 10 
- Delay (ms): 100 
- Don’t do anything else on this screen 

3. set-up SensorMarketSurvey_GUI_Assist_v1.3.1.vi 

- “Select Scans” tab 
    - Select IV 
- “Scan Settings” tab 
    - Sensor ID 
        - use serial number entered with barcode scanner 
        - Need to hit delete to remove the carriage return 
        - e.g 20UPGB42000055 
    - User comment 
    - Leave blank for now 
    - Performing group 
    - University of Glasgow 
    - Operator name 
    - Your name 

- Voltage Box 
    - Start: 0  
    - Step: -5V 
    - End: -200V 
    - Delay: 2s 

    <figure markdown>
    ![image info](/img/201_IV_Scans/IV-measure-control.png){ width="400" }
    <figcaption>Scan control screen</figcaption>
    </figure>

- Filenames 
    - D:\ITkPix\Preproduction\SERIAL_NUMBER_IV.txt 
    - e.g D:\ITkPix\Preproduction\20UPGB42000055_IV.txt 
    - D:\ITkPix\Preproduction\SERIAL_NUMBER_IT.txt 
    - D:\ITkPix\Preproduction\SERIAL_NUMBER_CV.txt 

    <figure markdown>
    ![image info](/img/201_IV_Scans/IV-measure-scan-gui.png){ width="400" }
    <figcaption>Ready for scans</figcaption>
    </figure>

### Loading the bare module 

- Use GelPak vacuum toll and electric vacuum pen to remove module from GelPak and place on the chuck 
- Use LabRemote to focus on the wirebond pads of the FE chip 
- Place Probe 2 – RHS onto Pad 11 of one FE chip 
    - Two pads up from the text “10”m, shown in figure labelled 11 
    - Place probe near edge of pad 
    - Clockwise is needle down 

    <figure markdown>
    ![image info](/img/201_IV_Scans/IV-measure-pads.jpg){ width="400" }
    <figcaption>Probe screen</figcaption>
    </figure>

- Place Probe 4 – LHS on back side of the sensor 
- Put chuck into Down position, close door, put in fine lift, turn out light 

### Running the software 

- Run the software:

    - K2700_T_RH_Monitor_v1.0.1.vi 
    - CurrentVoltageMeasurer_K237_v1.3.3.vi 
    - SensorMarketSurvey_GUI_Assist_v1.3.1.vi 

- Click “START Scans” in tab “Scan Settings” of SensorMarketSurvey_GUI_Assist_v1.3.1.vi 

- Copy the results file to: [Bare Module IV Sharepoint](https://gla.sharepoint.com/:f:/s/ATLASPixelITkQuadModules/EqR8S-u5PY1NnMazVSwAglMB1V9WuM37XVTnzMRw0s_fvQ?e=ezqxdx)
 

- Unload the system 

    - Turn on light 
    - Stage Down 
    - Open door 
    - Lift needles a little for safety for next module 
    - Use LabMaster to unload chuck 
    - Use vacuum tools to remove module and return to GelPak 

### Running analysis

TBA

### Upload to Production database 

TBA
