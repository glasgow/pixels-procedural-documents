# Dowel Check

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2024.02.29 | Kenneth Wraight | Created document |


## Scope

This is a regular part in the checking module flex PCB quality in Glasgow


## Purpose

Check dowel (?)


## Responsibilities

People: Sneha, Chris, Kenny


## Equipment

In lab:

- camera
- PC: can connect outside of lab
    - Data on [sharepoint](https://gla.sharepoint.com/sites/ATLASPixelITkQuadModules/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FATLASPixelITkQuadModules%2FShared%20Documents%2FSQ%5Fbareflexes%2FNCAB%2DLot1and%20Lot2%20Flexes&viewid=583b3f97%2D8ae1%2D4c8d%2D8a1e%2De46f6c0c3fc1)
        - *GLA_LayerThicknessMeasurements_XXX* excel sheet
        - _Template for DowelCheck_ tab has data in upload format
    - Upload with [Pixels webApp](https://itk-pdb-webapps-pixels.web.cern.ch)


## Procedure

### Measurements

TBC

### Uploads

When several measurements have been compiled on [sharepoint](https://gla.sharepoint.com/sites/ATLASPixelITkQuadModules/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FATLASPixelITkQuadModules%2FShared%20Documents%2FSQ%5Fbareflexes%2FNCAB%2DLot1and%20Lot2%20Flexes&viewid=583b3f97%2D8ae1%2D4c8d%2D8a1e%2De46f6c0c3fc1) you can upload the data via [Pixels webApp](https://itk-pdb-webapps-pixels.web.cern.ch)

- Login to webApp
    - using two database passwords

- Use righthand sidebar to choose:
    - theme: genericApp
    - page: Multi Test

- Add a _formatted_ file to the page (drag&drop or browse files with browser pop-up)
    - an example format can be down loaded using the _Downlad example_ button

- Cheack expected tests are uploaded to the page using the bar chart visualisation

- When ready click the _register TestRun_ button at the bottom of the page
    - contact Kenny if errors or difficulties with webApp

