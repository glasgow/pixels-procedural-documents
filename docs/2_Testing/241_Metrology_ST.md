# Metrology ST Scans

Scan ST using quad chuck 

## PC set-up

__PC login__

- username: Admin
- password: password

__Software__

SCAT ST on desktop


## Mechanical set-up

<figure markdown>
![image info](/img/241_Metrology_ST/metrology-module-on-jig.jpg){ width="400" }
<figcaption>Module on jig</figcaption>
</figure>

Remember to have parts clean and the vacuum _on_ 

Use three clamps to position the jig 

- LHS jig basically push to the edge 

<figure markdown>
![image info](/img/241_Metrology_ST/metrology-module-LHS.jpg){ width="400" }
<figcaption>Jig clamp LHS</figcaption>
</figure> 

<figure markdown>
![image info](/img/241_Metrology_ST/metrology-module-RHS.jpg){ width="400" }
<figcaption>Jig Clamp RHS</figcaption>
</figure>  

<figure markdown>
![image info](/img/241_Metrology_ST/metrology-module-top.jpg){ width="400" }
<figcaption>Jig clamp top</figcaption>
</figure> 

<figure markdown>
![image info](/img/241_Metrology_ST/metrology-module-side.jpg){ width="400" }
<figcaption>Jig side on</figcaption>
</figure> 


__Optical focus__

Focus with camera offset off on the Aluminium in the middle of the jig 

Have sensor set-up as distance measurement and maximum 

- I got –29,068 ; -42,687 ; 30,115 

Red dot in the middle of the green scale 

Use the auto focus button, and fine adjust, if required, with the Z button 


__Measurements__

On tool bar: Scan/New Worksheet 

Drive the CT100 to these start and stop points shown in the tables below

- Check and adjust if required 

Use line scan 

- Set scan step __2um in vertical, 1um in horizontal__

Between scans make sure New Worksheet on task bar is highlighted 

| Horizontal, Step 1um | Start | Stop | 
| -- | -- | -- | 
| 1 | -99,000 ; -81,000 | 38,600 ; -81,000 | 
| 2 | -99,000 ; -61,000 | 38,600 ; -61,000 | 
| 3 | -99,000 ; -22,000 | 38,600 ; -22,000 | 
| 4 | -99,000 ; -2,000 | 38,600 ; -2,000 | 
 

| Vertical, Step 2um | Start | Stop | 
| -- | -- | -- | 
| 1 | -69,000 ; -107,000 | -69,000 ; 25,741 | 
| 2 | -49,000 ; -107,000 | -49,000 ; 25,741 | 
| 3 | -11,000 ; -107,000 | -11,000 ; 25,741 | 
| 4 | 11,000 ; -107,000 | 11,000 ; 25,741 | 

If there is missing data, you might have hit a dowel pin

- Move the scan slightly and re-do

If there is a small local jump in the data you might have hit dust

- Move the scan slightly and re-do

Vertical distance is 18,000 to 58,000 = 40000 = 40mm 

The distances were found by driving the max distances while in CHRocodile E mode 

Doing a vertical and horizontal scan and then working out where lines are that are 10mm in from each edge

- Missing dowels as best as possible 

Save scan:

- Rename the scan Vertical# or Horizontal# as appropriate. 
- Save the scan file .scnx to C:/data/ITkPix/Bare_yyyymmdd_n 

__Data Analysis__

For each scan you get a plot like this: 

<figure markdown>
![image info](/img/241_Metrology_ST/metrology-analysis-plot.png){ width="400" }
<figcaption>Analysis plot with bands</figcaption>
</figure> 

Two modules are scanned at the same time. 

Define the reference cursor for the three areas shown which correspond to the Aluminum jig 

Define the Primary cursor for the two areas where there is silicon. 

Use Templates:

- BareModuleHeights_H 
- BareModuleHeights_V 
- Adjust the cursors 
- Press Normalise 

Report:

<figure markdown>
![image info](/img/241_Metrology_ST/metrology-analysis-table.png){ width="400" }
<figcaption>Analysis report</figcaption>
</figure> 

Save the scan file:

- Use Publish to Excel tab to save as an excel spreadsheet 
- Click on Horizontal1 and export. 
- Then Horizontal 2 and export, 3.. , 4.. then vertical 1... 
- Finally Vertical 4. 

Save the scan file. 

Each Export goes to the same excel file. 

Save this file as Data.cvs and Data.xlsx into the directory C:/data/ITkPix/Bare_yyyymmdd_n 

In this directory create a file called SN.csv and SN.xlsx (copy from template directory C:/data/ITkPix/Template) 

With this format:

| SN1 | SN2 |
| -- | -- |
| SN3 | SN4 |

To represent the 4 modules scanned. 

 
Copy the file AverageHeights.xlxs from the template directory and open it up, along with the data.csv and SN.cvs files

- You will need to press F9 


Copy directory to SharePoint 

- Update module tracker xls on sharepoint 

 

 