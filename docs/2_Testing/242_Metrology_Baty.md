# Metrology Baty Scans

Baty-XY dimension measurement using quad chuck 

## PC set-up

<!-- __PC login__
- username: Admin
- password: password -->

__Software__
Baty Fusion - link on desktop

## Mechanical set-up


<figure markdown>
![image info](/img/242_Metrology_Baty/metrology-baty-chuck.jpg){ width="400" }
<figcaption>Module on jig</figcaption>
</figure>

Remember to have parts clean and the vacuum on

__Optical focus__

In the window of Baty Fusion Software, on tool bar Switch Camera On/Off to enable the camera and the window of Camera Measurement  

Moving the joystick:

- in X/Y direction is to move the stage
- rotating the joystick is to move the camera up/down
- +  is faster speed, - is slower

Focus on the edge of the module

In the window of Camera Measurement, adjust the lighting by clicking the Light Control icon, adjust brightness/ contrast, etc. by clicking the Camera Controls icon to get good image.  


__Measurements__

In the window of Camera Measurement, Choose Zoom level as MIN( there are MIN, 2 3 4 5 MAX), and choose Line measurements.

Locate the cursor at the edge of the sensor

Left click one point on the edge and left click the other point to define a line. 

Then drag the line to form a narrow rectangle where the points on the line taken are shown in the Line Measure box

click OK when most points are inline otherwise Delete and retake the points

Do the same thing to take the points at the edge of the chip. 

<figure markdown>
![image info](/img/242_Metrology_Baty/metrology-baty-edge.png){ width="400" }
<figcaption>Click two points on the edge of the sensor to form a line</figcaption>
</figure>

<figure markdown>
![image info](/img/242_Metrology_Baty/metrology-baty-drag.jpg){ width="400" }
<figcaption>Drag the line into a rectangle to take the points on the line</figcaption>
</figure>

<figure markdown>
![image info](/img/242_Metrology_Baty/metrology-baty-measure.jpg){ width="400" }
<figcaption>Points taken on the edge of the sensor</figcaption>
</figure>

Move the joystick to the other side of the module to define the edge of the sensor and the chip there. 

Then you get 4 vertical lines shown in the XY plane

- The two inner lines are the edges of the sensor
- the outer lines are for the chip.  

Click the two inner lines, and left click to choose Horizontal you will get the distance between the two edges of the sensor

- that is Sensor x. 

In the same way, you get FEx. 

Repeat the steps above you can get Sensor y, FE y. Remember to choose vertical when doing the y measurement.  

Do Sensor x, y FE x, y measurement for all of the four modules loaded on the gig. 

Tips: 

 - there is zoom out icon on the lower left side. For zoom-in you right click the area you choose. 
 - There is an eraser to delete the lines you don’t want to keep. 
 - Be careful when eraser is ON. You can undo if you accidentally erase some lines. 
 - The numbers shown on the lower left side are for the lines, click the arrows the line will be highlighted by red color corresponding to the line number.

<figure markdown>
![image info](/img/242_Metrology_Baty/metrology-baty-xyz.jpg){ width="400" }
<figcaption>Distance measurement between two lines</figcaption>
</figure>

<figure markdown>
![image info](/img/242_Metrology_Baty/metrology-baty-xyz.jpg){ width="400" }
<figcaption>Bare module with FE chips side in contact with chuck</figcaption>
</figure>

__Save Data__

Save data in Excel 

After finish the Sensor x, y FE x, y measurement for all of the four modules.

Click Print icon on  the tool bar in the window of Baty Fusion Software. 

In the print box, tick Tabulated Dimensions and XY view. 

First Clear Report and then Add to Report. 

Click the icon Output Data to Excel. Then you get the measurement data as below saved in an excel file. 

The notes I put below the form show the IDs of the modules measured. 

<figure markdown>
![image info](/img/242_Metrology_Baty/metrology-baty-table.png){ width="400" }
<figcaption>Data table</figcaption>
</figure>


__Save Graph__

Under Print box, tick Graphic Details and XY view. Some measurements are overlapped shown in the graph.

<figure markdown>
![image info](/img/242_Metrology_Baty/metrology-baty-graph.png){ width="400" }
<figcaption>Data graph</figcaption>
</figure>

Use a flash drive to download the files and upload them in the sharepoint: 

- ATLAS Pixel ITk Quad Modules/Bare Module/Metrology/ XY measurement with Baty
- (how to name the file?)


__Unload the modules__

Make sure put each of them back in the case with the right ID
