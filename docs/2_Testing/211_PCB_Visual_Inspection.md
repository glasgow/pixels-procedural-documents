# PCB Visual Inspection

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2024.02.28 | Kenneth Wraight | Created document |
| v2 | 2024.12.17 | Kenneth Wraight | Created document |


## Scope

This is a regular part in the checking module flex PCB quality in Glasgow


## Purpose

Check flexes are clean and without defects


## Responsibilities

People: Sneha, Chris, Kenny


## Equipment

In lab:

- camera
- PC: can be connected outside of lab
    - localDB
    - module-qc-nonelec-gui

## Documentation

Using [VI GUI](https://indico.cern.ch/event/1355427/contributions/5706803/attachments/2768526/4822990/VASLIN_KEK_Visual_inspection.pdf) (KEK): indico meeting Dec 2023

## VI GUI Set-up

### Installation

Instructions for initial installation.

- [module-qc-nonelec-gui repo](https://gitlab.cern.ch/atlas-itk/pixel/module/module-qc-nonelec-gui)

    - tag **v0.0.9rc2** for usable version (requires python 3.12)
    - tag **edinburgh-vi** for Alex's version: use frameless (cropped) 4x4 regions

        - made by editting src/module_qc_nonelec_gui/qc_tests/VISUAL_INSPECTION/config/config_PCB.json

- open virtual environment ([cheat sheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf))

    > conda create --name pcb python=3.12

    > conda activate pcb
    

- install GUI (in virtual environment)

    > git clone https://gitlab.cern.ch/hoide/module-qc-nonelec-gui.git

    > cd module-qc-nonelec-gui

    > git checkout edinburgh-vi

    > git pull

    > python3 -m pip install -e .

- add _golden module_ data from [cernbox](https://cernbox.cern.ch/index.php/s/CpmUXM1wyKuIWIg)

    - unpack and format as zip file (I dunno why it's not in the expected format)

    > gunzip golden_module.tar.bz

    > tar -xvf golden_module.tar

    > zip -r golden_module.zip golden_module

    - this should provide golden_module.zip file to use if prompted when running GUI for first time

### Check GUI runs

Check application opens.

- link to localDB

    - URL: http://194.36.1.20:5000/localdb/

        - Need to pull components form PDB

    - mongodb

        - URL: http://194.36.1.20:27017

- access localDB info.

    ??? localDB access

        user: rd53a

        password: _the usual fb11_


## Procedure

Instructions for normal running.

### Get images

1. Take image of bare flex and save somewhere accessible e.g. Sharepoint

- [camera instructions](https://glasgow-itk-pixels-sop.docs.cern.ch/4_Miscellany/402_Nikon_Camera/)

2. Copy images to ppepc76

- Use Cernbox or copy with anydesk

### Running QC GUI

1. Access ppepc76 (rm. 341)
    
    - local access:

    ??? local

        usr: ppe_root

        pwd: fb_11

    
    - remote access:

    ??? remote

        anydesk: 956345906

        login: fb_11

2. start Visual Inspection GUI:

    - Go to GUI directory:

    > cd ~/alien_repositories/module-qc-nonelec-gui

    - start GUI:

    > module-qc-nonelec-gui &

2. Select PDB component & test

    a. Connect to __localDB__

     - same username and password as logging into [localDB](http://194.36.1.20:5000/localdb/)


    ??? localDB access

        user: rd53a

        password: _the usual fb11_

    <figure markdown>
    ![image info](/img/211_PCB_Visual_Inspection/qc-gui-login.png){ width="400" }
    <figcaption>Login box</figcaption>
    </figure>

    b. Select component by serialNumber
    
     - select via drop down or paste serialnumber into the box
     - then press "next"

     __NB__ make sure localDB is up to date with Production Database

    ??? note "update localDB"

        Once logged-in:

        - got to _Top_ page
        - input (partial) serialNumber
        - click to get component data from PDB

    <figure markdown>
    ![image info](/img/211_PCB_Visual_Inspection/qc-gui-select-sn.png){ width="400" }
    <figcaption>Select component serial number</figcaption>
    </figure>

    c. Select testType

     - then press "next"

    <figure markdown>
    ![image info](/img/211_PCB_Visual_Inspection/qc-gui-select-test.png){ width="400" }
    <figcaption>Select test type</figcaption>
    </figure>

4. Set-up test

    a. Input properties: Inspector & instrument

    <figure markdown>
    ![image info](/img/211_PCB_Visual_Inspection/qc-gui-vi-setup.png){ width="400" }
    <figcaption>Set test properties</figcaption>
    </figure>
    
    b. Select image from Browser pop-up - should see _black & white_ image appear

     - At this point the green circles are not important

    <figure markdown>
    ![image info](/img/211_PCB_Visual_Inspection/qc-gui-vi-first.png){ width="400" }
    <figcaption>Initial image</figcaption>
    </figure>

    c. Orient by selecting centre of GA1 and GA3

     - should see green markers: PCB centre, GA1 & GA3 centres, corners of PCB
     - then press "next"

    <figure markdown>
    ![image info](/img/211_PCB_Visual_Inspection/qc-gui-vi-orient.png){ width="400" }
    <figcaption>Orientated image</figcaption>
    </figure>

    d. Start inspection

     - should see _colour_ image with 8x8 grid overlayed

    <figure markdown>
    ![image info](/img/211_PCB_Visual_Inspection/qc-gui-vi-colour.png){ width="400" }
    <figcaption>Grid to assess image</figcaption>
    </figure>

