# Soak Test

| Revision | Date | Editor | Contents |
| -- | -- | -- | -- |
| v1 | 2024.04.15 | Kenneth Wraight | Created document |

## Scope

This part of module flex PCB quality assurance in Glasgow


## Purpose

Long term functionality


## Responsibilities

People: Sneha, Liam, Kenny


## Equipment

In lab (GLADD2):

- PC

- Pico Logger

- LV PS

## Procedure

__TBD__

1. Take data

2. Correct data

??? note "Get scripts repo."

    Access them as follows:

    > git clone https://gitlab.cern.ch/glasgow/pixels-procedural-scripts.git

    > cd strips-procedural-scripts/Testing/214_PCB_Soak_Tests/

    Then use jupyter or equivalent (e.g. VisualStudio) to run the scripts.
