# Useful Links

__Glasgow__ ITk Standard Operation Procedure

### Revision history
| Revision | Date | Editor | Contents |
| --- | --- | --- | --- |
| v1 | 2024.02.28 | Kenneth Wraight  |  Created document |

A list of links with some order.

## Official Documentation

__"Made by experts"__

### General

[ITk-docs](https://itk.docs.cern.ch)



## Unofficial Documentation

__"Made by users"__

Glasgow procedure documents on [sharepoint](https://gla.sharepoint.com/sites/ATLASPixelITkQuadModules/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FATLASPixelITkQuadModules%2FShared%20Documents%2FProcedures&p=true&ga=1)

- this includes (as of 29/02/2024):

    - Bare Module Metrology
    - Sensor IVs
    - Nikon Camera use



## Miscellaneous



