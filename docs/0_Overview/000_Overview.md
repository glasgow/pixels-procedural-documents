# Master SOP

__Glasgow__ ITk Standard Operation Procedure

### Revision history
| Revision | Date | Editor | Contents |
| --- | --- | --- | --- |
| v1 | 2023.10.02 | Kenneth Wraight  |  Created document |
| v2 | 2024.02.28 | Kenneth Wraight | Major revision |

This master document describes the operations at Glasgow for manufacturing and testing pixel modules within the _ATLAS ITk Phase-II_ upgrade project. It describes responsibilities, local organization and the documentation structure.

## Scope

The ATLAS group at Glasgow hosts one of the manufacturing sites for ATLAS ITk pixel modules to be used in the upgraded ATLAS Inner tracker detector. Most of the activities will be carried out in the GLADD1 and GLADD2 labs. 

## Purpose

This document and the herein listed subdocuments describe the procedures in place to manufacture ITk Pixel modules to high standards and principles to assure their quality.


## Definitions 

The definitions defined here apply to all sub-documents as well.

| Abbr. | Expansion | meaning |
| -- | -- | -- |
| ITk | Inner Tracker | Consists of pixel and strip trackers |


## Responsibilities

Includes:

 - the __main responsible__ is the _team leader_
    - Currently: Andy Blue 
 - the __responsible for operations__ maintains the SOP documents current, organizes the proper training and makes sure all members of the team follow the SOP. Reports to the _main responsible_.  
    - Currently:  ??
 - __team members__ are responsible to follow the instructions described in the SOP documents for which they are trained. They are not allowed to carry out any procedure without proper and documented training. Pre-production is considered training phase where procedures are allowed to be carried out without documented training. Team members report to the _responsible for operations_
    - The list of current _team members_ is maintained by the _responsible for operations_


## Document hierarchy

The documents are organised in a hierarchical way:

<figure markdown>
   ![image info](/img/000_Overview/SOPhierarchy.svg){ width="500" }
  <figcaption>SOP documentation map</figcaption>
</figure>

Documents are identified by a 3-digit number. The top-level document has the number 000. All the other documents carry numbers different from this. The first digit identifies the group the document belongs to.
 
### Referencing documents

- Cross-reference within SOP documents go as SOP~XYZ, where XYZ is the document number. This assumes a reference to the latest version of that document. References to specific version are to be avoided.

    - X is relevant area: e.g. assembly, testing, etc.
    - Y is for component type: 

    | __Y__ | 0 | 1 | 2 | 3 | 4 | 5 |
    | -- | -- | -- | -- | -- | -- | -- |
    | __type__ | General | PCB | Front End | Sensor | Bare Module | Module |


- Referencing from external documents should use the full title and SOP number.

### List of documents

Table shows the existing documents. A list of all current documents is to be maintained and made accessible to all people involved.

