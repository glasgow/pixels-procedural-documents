# Welcome Glasgow ITk Pixels Standard Operating Procedures Documentation

This documentation is intended to help standardise operating procedures for Glasgow ITk Pixels work.
 
Use the [git repo.](https://gitlab.cern.ch/glasgow/pixels-procedural-documents) to make updates.

